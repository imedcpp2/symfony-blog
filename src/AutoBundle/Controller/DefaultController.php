<?php

namespace AutoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        return $this->render('@Auto/Default/index.html.twig');
    }

    /**
     * @Route("/offre", name="nos_offre")
     */
    public function offreAction()
    {
        $donnes = $this->getDoctrine()->getRepository('AutoBundle:Voiture');
        $voitures=$donnes->findAll();
        return $this->render('@Auto/Default/offre.html.twig',['voitures' => $voitures]);
    }
}
