<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $identite="Toto Momo";
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', ['ident'=>$identite]);
    }
    /**
     * @Route("/donnes", name="donnes")
     */
    public function donneesAction(){
        $autos = [
            ['id'=>1,'marque'=>"BMW",'model'=>"X3",'pay'=>'Allmagne','url'=>"image/001.jpg"],
            ['id'=>2,'marque'=>"Porche",'model'=>"Panemera",'pay'=>'Allmagne','url'=>"image/001.jpg"],
            ['id'=>3,'marque'=>"Volvo",'model'=>"XC70",'pay'=>'Suéde','url'=>"image/001.jpg"],
            ['id'=>4,'marque'=>"Citroéne",'model'=>"C3",'pay'=>'France','url'=>"image/001.jpg"]            
        ];
        return $this->render('default/donnes.html.twig',['autos'=>$autos]);
    }
    /**
     * @Route("/exp/{param}", name="exemple",defaults={"param"="gg"})
     * @Method("GET")
     */
    public function exempleAction(Request $request){

        var_dump($request->query->all()); die;       
        return $this->render('default/exp.html.twig');
        

    }
}
