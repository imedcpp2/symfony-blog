<?php
namespace AppBundle\Menu;
use Knp\Menu\MenuFactory;
class Builder{

    public function monMenu(MenuFactory $factory, array $options){
        
        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class','nav navbar-nav');
        $menu->addChild('Acceuil', ['route'=>'homepage']);
        $menu->addChild('Nos offre', ['route'=>'nos_offre']);
        
        return $menu;
    }

}